import { I18nUtil } from 'rainbowui-desktop-core';

try {
	module.exports = require("./reactjs-tag.i18n." + I18nUtil.getSystemI18N());
} catch(exception) {
	module.exports = require("./reactjs-tag.i18n.en_US");
}
