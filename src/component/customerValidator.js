import {ValidatorContext} from 'rainbow-desktop-cache';
import r18n from "../i18n/reactjs-tag.i18n";

const InputValidator = {

    validate: function(component){
        let flag = false;
        let validator = {
            // notEmpty: {
            //     message: '字段不能是空'
            // },
            stringLength: {
                min: 18,
                max: 18,
                message: r18n.stringLength
            },
            regexp: {
                regexp: /^[1-9]\d{5}(18|19|([23]\d))\d{2}((0[1-9])|(10|11|12))(([0-2][1-9])|10|20|30|31)\d{3}[0-9Xx]$/,
                message: r18n.regexp
            }
        };
        ValidatorContext.putValidator(component.getValidationGroup(), component.componentId, validator);
    }
	

};

module.exports = InputValidator;
